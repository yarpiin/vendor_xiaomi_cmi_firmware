# Firmware
PRODUCT_COPY_FILES += \
vendor/xiaomi/cmi/firmware/cmnlib64.mbn:install/firmware-update/cmnlib64.mbn \
vendor/xiaomi/cmi/firmware/xbl_config_5.elf:install/firmware-update/xbl_config_5.elf \
vendor/xiaomi/cmi/firmware/NON-HLOS.bin:install/firmware-update/NON-HLOS.bin \
vendor/xiaomi/cmi/firmware/cmnlib.mbn:install/firmware-update/cmnlib.mbn \
vendor/xiaomi/cmi/firmware/BTFM.bin:install/firmware-update/BTFM.bin \
vendor/xiaomi/cmi/firmware/km4.mbn:install/firmware-update/km4.mbn \
vendor/xiaomi/cmi/firmware/xbl_5.elf:install/firmware-update/xbl_5.elf \
vendor/xiaomi/cmi/firmware/tz.mbn:install/firmware-update/tz.mbn \
vendor/xiaomi/cmi/firmware/aop.mbn:install/firmware-update/aop.mbn \
vendor/xiaomi/cmi/firmware/featenabler.mbn:install/firmware-update/featenabler.mbn \
vendor/xiaomi/cmi/firmware/xbl_config_4.elf:install/firmware-update/xbl_config_4.elf \
vendor/xiaomi/cmi/firmware/storsec.mbn:install/firmware-update/storsec.mbn \
vendor/xiaomi/cmi/firmware/uefi_sec.mbn:install/firmware-update/uefi_sec.mbn \
vendor/xiaomi/cmi/firmware/qupv3fw.elf:install/firmware-update/qupv3fw.elf \
vendor/xiaomi/cmi/firmware/abl.elf:install/firmware-update/abl.elf \
vendor/xiaomi/cmi/firmware/dspso.bin:install/firmware-update/dspso.bin \
vendor/xiaomi/cmi/firmware/devcfg.mbn:install/firmware-update/devcfg.mbn \
vendor/xiaomi/cmi/firmware/xbl_4.elf:install/firmware-update/xbl_4.elf \
vendor/xiaomi/cmi/firmware/hyp.mbn:install/firmware-update/hyp.mbn \
vendor/xiaomi/cmi/firmware/cmnlib64.mbn:install/firmware-update/cmnlib64.mbn \
vendor/xiaomi/cmi/firmware/cmnlib.mbn:install/firmware-update/cmnlib.mbn \
vendor/xiaomi/cmi/firmware/tz.mbn:install/firmware-update/tz.mbn \
vendor/xiaomi/cmi/firmware/aop.mbn:install/firmware-update/aop.mbn \
vendor/xiaomi/cmi/firmware/storsec.mbn:install/firmware-update/storsec.mbn \
vendor/xiaomi/cmi/firmware/qupv3fw.elf:install/firmware-update/qupv3fw.elf \
vendor/xiaomi/cmi/firmware/abl.elf:install/firmware-update/abl.elf \
vendor/xiaomi/cmi/firmware/devcfg.mbn:install/firmware-update/devcfg.mbn \
vendor/xiaomi/cmi/firmware/hyp.mbn:install/firmware-update/hyp.mbn \
